function [algo] = bayeta( neuronTraces, neuronEvents, sampleRate, evenOutputCSV, traceOutputCSV, createPlots, TAU, DoGSigma, eventCenter)
try
    VERSION = 1.7;
    
    import NPE.DataIO
    import NPE.Plot
    import NPE.Filter
    
    if nargin ~= 9
        usage();
        return
    end
    
    %%% Algorithm Parameters in [sec]
    TREND_WINDOW = 120;    
    EVENT_WINDOW = 5;
    
    fprintf('Running bayeta v%.02f ... \n', VERSION);
    
    %%% Load traces
    checkFile(neuronTraces, 'r')    
    T = DataIO.readNeuroTraces(neuronTraces, 'CN');
    [r, c] = size(T);
    E = DataIO.readNeuronEvents(neuronEvents, 'ORIGINAL', 'NE');
    Etimes = getEventTimes(E, 2:c);
    algo = T;
    
    outEvent = T;
    outTrace = T;
    
    %Generate Plot containing all traces
    overview = Plot.plotTracesTable(T, 20, 'Trace Overview');
    [f1,f2,f3] = fileparts(neuronTraces);
    pn = sprintf('%s-overview.png', f2);
    fprintf('Generating overview plot %s ...\n', pn);
    print(pn, '-dpng', '-r500');
    close(overview)
    
%     cumTrace = zeros(r, 1);
%     for l = 2:c
%         trace = T.(l);
%         trace = trace / max(trace);
%         cumTrace = cumTrace + trace;
%     end
%     cumTrace = cumTrace / (c-1);
%     plot(cumTrace);
    
    % Process each trace in the file
    for l = 2:c    
        try
            fprintf('Processing column %d/%d ...\n', l-1, c-1);

            %% Process data
            originalTrace = T.(l);        
            orignalTrend = NPE.Filter.getTrend(originalTrace, TREND_WINDOW*sampleRate);                               
            detrended = NPE.Filter.comprensateForTrend(originalTrace, orignalTrend);        
            convoluted = NPE.Filter.expConvolution(detrended, TAU*sampleRate);
            events = NPE.Filter.DoGEventDetection(convoluted, TAU*2*sampleRate, TAU*50*sampleRate, DoGSigma, eventCenter);
            events = NPE.Filter.filterEvents(events, TAU*5*sampleRate);
            
            if createPlots
                %% Generate Plots            
                f = figure('visible','off');
                %f = figure();
                plot(T.CNTime, convoluted);
                hold on                
                
                %Draw detected events
                lineH = max(convoluted)*1.1;
                lineL = max(convoluted)*0.9;
                for i = 1:length(events);
                    e = events(i);
                    line([T.CNTime(e) T.CNTime(e)],[lineL lineH], 'Color', 'black', 'LineStyle', '-');
                end

                %Draw Events in event inputfile
                iE = Etimes(:, l-1);
                for i = 1:length(iE)
                    if ~isnan(iE(i))
                        line([iE(i) iE(i)],[lineH lineH], 'Color', 'black', 'LineStyle', '-', 'Marker', 'v', 'MarkerFaceColor', 'blue');
                    end
                end
                
                title(sprintf('Bleach compensated Calcium Signal %s', strrep([T.Properties.VariableNames{l} '.png'], '_', ' ')))
                grid on
                xlabel('Time')
                ylabel('Traces')       
                legend('show')
                legend(['Filtered and Bleach compensated Signal Sigma ', num2str(DoGSigma)])        
                %legend('Original Signal', 'Original Trend', 'Trend Compensated', 'New Trend', 'ExpConv Filtered', 'DoG Filtered')

                fn = strsplit(neuronTraces, '.');
                pn = sprintf('%s_%s.png', fn{1}, T.Properties.VariableNames{l});
                fprintf('Generating plot %s ...\n', pn);
                %print(pn, '-dpng', '-r600');
                saveas(f, pn);
                close(f)
            end
        
            %%% Generate output Matrix
            outEvent.(l) = zeros(r, 1);
            outEvent{events, l} = ones(length(events), 1);

            outTrace.(l) = detrended;
            catch err
                warning('Error Processing column %d! %s', l, err.message)
        end
    end
    
    fprintf('Writing Events to %s ... \n', evenOutputCSV);
    T2 = table2array(outEvent);
    csvwrite(evenOutputCSV, T2);
    
    fprintf('Writing Traces to %s ... \n', traceOutputCSV);
    T2 = table2array(outTrace);
    csvwrite(traceOutputCSV, T2);
    
    fprintf('Finished!\n');    
catch err
    warning('Extracting Neuro Events failed! %s', err.message)
end
end

function checkFile(fileName, permission)
    fileID = fopen(fileName, permission);
    if fileID == -1
        error('BioComp:FileCheckFailed', 'Failed to check file [%s] for [%s]', fileName, permission)
    else
        fclose(fileID);
    end
end

function events = getEventTimes(eventTable, columns)
    [r, c] = size(eventTable);
    events = nan(r, length(columns));
    times = eventTable.(1);
    maxL = 0;
    for i = 1:length(columns)
        %name = eventTable.Properties.VariableNames{i};
        eTimes = times(eventTable.(columns(i)) > 0);
        l = length(eTimes);
        events(1:l, i) = eTimes;
        if l > maxL
            maxL = l;
        end
    end
    events = events(1:maxL, :);
end




function threshold = getThreshold(X, windowSize, thresholdScaling)
    assert(windowSize < length(X))
     
    %Extend X by windowSize before and after
    t1 = flip(X(1:(windowSize*1.0)));
    t2 = flip(X(end-(windowSize/2):end));
    data = vertcat(t1, X, t2);
    
    %smoother = fspecial('average', [1, windowSize]);
    smoother = fspecial('gaussian', [1, windowSize], windowSize / 6.0);
    
    trend = filter(smoother, 1, data);
    
    %Cut off beginning of trend, and shift it by windowSize/2
    trend = trend((windowSize*1.5)+2:end);    
    
    %Calculate moving stdandart deviation    
    S = filter(ones(1, windowSize), 1, trend);   
    S = trend - (S/windowSize);
    S = power(S, 2);
    S = S / windowSize;
    S = sqrt(S);
    
    S = filter(fspecial('average', [1, windowSize]), 1, trend);
    %S = abs(S);
    
    threshold = trend + S*thresholdScaling;
    %threshold = S*thresholdScaling;
end

function oddV = makeOdd(value)
    value = int16(value);
    if mod(value, 2)
        oddV = value;
    else
        oddV = value + 1;
    end
    %oddV = double(oddV);
end

function madThreshold = getMADThreshold(X, windowSize, madScaling)
    assert(windowSize < length(X))    
    windowSize = makeOdd(windowSize);
    
    X = smooth(X, makeOdd(15));
    
    %%% Calculate a sliding MAD
    slidingMAD = zeros(length(X),1);
    for l = 1:length(X)
        b = max(l-(windowSize/2), 1);
        e = min(l + windowSize/2, length(X));                
        slidingMAD(l) = mad(X(b:e));
    end
        
    %%% Find inflection points    
    s = sign(diff([X(1); X]));    
    inflectionPoints = find( diff([s(1); s]) ~= 0);
    inflectionPoints = inflectionPoints - 1;
    inflectionPoints(1) = 1;
    %inflectionPoints = inflectionPoints + 2;%Compensate for the two diffs
    direction = s(inflectionPoints);       
    
    madThreshold = zeros(length(X), 1);  
    
    %%% Generate MAD Threshold by going from inflection point to inflection
    %%% point
    prev = 1;
    for i = 1:length(inflectionPoints)
        infP = inflectionPoints(i);
        if(direction(i) < 0)
            %PN = infP; 
            madThreshold(prev:infP) = madThreshold(prev);
            prev = infP;
        else
            NP = infP;
            madThreshold(prev:NP) = X(prev) + slidingMAD(prev)*madScaling;    
        end
    end
    
    madThreshold = smooth(madThreshold, windowSize);       
end

function usage()
    fprintf('Usage: bayeta( TraceInputFile, EventInputFile, SampleRate, EventOutput, TraceOutput, generatePlots, TAU, DoGSigma, eventCenter);\n\n');
    fprintf('Example: bayeta("24-10-EPMa-traces.csv", "24-10-Events.csv", 15, "24-10-EPMa-NewEvents.csv", "24-10-EPMa-NewTraces.csv", true, 1.5, 3.0, "onSet");\n');
    fprintf('24-10-EPMa-traces.csv ... Input file containing the traces from Mosaic\n');
    fprintf('24-10-Events.csv ... Input file containing the event data from Mosaic\n');
    fprintf('15 ... Sample Rate\n');
    fprintf('24-10-EPMa-NewEvents.csv ... Output file that will contain the events (Can be fed into gafas)\n')
    fprintf('24-10-EPMa-NewEvents.csv ... Output file that will contain the traces (Can be fed into gafas)\n')
    fprintf('true ... indicate if to generate plots for each trace\n');
    fprintf('1.5 ... defines a decay time of 1.5 seconds for the calcium event\n');
    fprintf('3.0 ... detect events that are 3.0 * std of the signal\n');
    fprintf('"onSet" ... define the event to be the onset of a spike (other value, center ... the event is defined by the center of a spike. offSet ... the falling spike)\n');
end