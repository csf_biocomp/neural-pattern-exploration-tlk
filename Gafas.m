function [ HA ] = Gafas( ethoFile, GPIOFile, GPIOTimeMultiplyer, NeuronContinual, NeuronEvents, typeDefMap, nexFilePath, SampleRate)
    VERSION = 0.8;
    
    import NPE.DataIO
    fprintf('Running Gafas v%01.2f ...\n', VERSION);
    
    % start new nex file data, 40k is hard coded, and setting the actual
    % sample rate will make the file unusable in NeuroExplorer ...
    nexFile = nexCreateFileData(40000);    
    
    if isempty(typeDefMap)
        dTypes = containers.Map();
    else
        load(typeDefMap, 'dTypes')
    end
    
    if ~isempty(ethoFile)        
        fprintf('#################### Adding Anymaze/EthoVision File #######################\n');
        ethoVision = readTraceFile(ethoFile);
        ethoVision = findColumnTypes(ethoVision, dTypes);
        nexFile = addTable2Nex(nexFile, ethoVision, SampleRate);
    end
                 
    if ~isempty(NeuronContinual)        
        fprintf('#################### Adding Neuronal data File #######################\n');
        NCTable = DataIO.readNeuroTraces(NeuronContinual, 'Neuron_Continous');
        NCTable = findColumnTypes(NCTable, dTypes);
        nexFile = addTable2Nex(nexFile, NCTable, SampleRate);
    end   
    
    if ~isempty(NeuronEvents)        
        fprintf('#################### Adding Neuronal data File #######################\n');        
        NCTable = DataIO.readNeuronEvents(NeuronEvents, 'Neuron_Filtered', 'Neuron_Events');
        NCTable = findColumnTypes(NCTable, dTypes);
        nexFile = addTable2Nex(nexFile, NCTable, SampleRate);                
    end
    
    if ~isempty(GPIOFile)    
        fprintf('#################### Adding GPIO File #######################\n');
        if GPIOTimeMultiplyer == 0
            GPIOTable = addGPIOData(GPIOFile, 1.0);
            t1 = GPIOTable{end, 1};
            t2 = NCTable{end, 1};
            GPIOTimeMultiplyer = t2 / t1;
        end
        GPIOTable = addGPIOData(GPIOFile, GPIOTimeMultiplyer);
        GPIOTable = findColumnTypes(GPIOTable, dTypes);
        nexFile = addTable2Nex(nexFile, GPIOTable, 0);
    end            

    
    fprintf('################## Writing Result file ... ########################\n');
    %fprintf('Writing Output nex file to %s\n', nexFilePath)
    %writeNexFile(nexFile, nexFilePath );
    fprintf('Writing Ouput nex5 file to %s\n', [nexFilePath '5'])
    writeNex5File(nexFile, [nexFilePath '.nex5'] );
end

function nexFile = addTable2Nex(nexFile, T, SampleRate)
try            
    nColumns = length(T.Properties.VariableNames);
    t = []; %Holds the timestamps for this table
    for i = 1:nColumns
        name = T.Properties.VariableNames(i);
        name = name{1};
        type = T.Properties.VariableUnits(i);
        type = type{1};
        data = T.(i);
                
        switch type
            case 'T'
                fprintf('Time column ...\n');
                t = data; %Set local table time
            case 'C'
                fprintf('Adding continues data %s\n', name);
                nexFile = nexAddContinuous(nexFile, t(1), SampleRate, data, name);
            case {'B', 'E'}
                fprintf('Adding binary data %s\n', name);
                % add event spike train
                eventTs = t(find(data == 1));
                if isempty(eventTs)
                    sprintf('%s has no events. Will add a virtual event at timepoint 0', name)
                    eventTs = t(1);
                end
                nexFile = nexAddEvent(nexFile, eventTs, name);
            case 'I'
                fprintf('Adding Interval %s\n', name);
                [onSet, offSet] = creatInterval(t, data);
                nexFile = nexAddInterval(nexFile, onSet, offSet, name);
            case 'N'                
                fprintf('Adding Neuron data %s\n', name);
                % add event spike train
                eventTs = t(find(data == 1));
                if isempty(eventTs)
                    sprintf('%s has firing events. Will add a virtual fireing at timepoint 0', name)
                    eventTs = t(1);
                end
                nexFile = nexAddNeuron(nexFile, eventTs, name);
            case '?'
                fprintf('Unknown data type for column %s!\n', name);
            case 'X'
                fprintf('Filtering column %s', name);
        end
    end
catch err
    warning('Adding Table to Nex File failed! %s', err.message)
end

end

function T = addGPIOData(GPIOFile, GPIOTimeMultiplyer)
try
    GPIO =  csvread(GPIOFile);    
    D1 = diff([0; GPIO(:,2)]); %Assuming basic state to be 0
    D2 = diff([0; GPIO(:,3)]); %Assuming basic state to be 0
    D1_onSet = D1 == 1;
    D1_offSet = D1 == -1;    
    D1_onSet = 0 + D1_onSet;
    D1_offSet = 0 + D1_offSet;
    
    D2_onSet = D2 == 1;
    D2_offSet = D2 == -1;    
    D2_onSet = 0 + D2_onSet;
    D2_offSet = 0 + D2_offSet;
    
    X = GPIOTimeMultiplyer;
    names = {'GPIOTime'; 'GPIO1'; 'GPIO2'; 'GPIO1_ONSET'; 'GPIO2_ONSET';'GPIO1_OFFSET'; 'GPIO2_OFFSET'};
    T = table(GPIO(:,1)*X, GPIO(:,2), GPIO(:,3), D1_onSet, D2_onSet, D1_offSet, D2_offSet, 'VariableNames', names);
catch err
    warning('Adding GPIO data failed! %s', err.message)
end
end

function [onSet, offSet] = creatInterval(time, data)
    D = diff([0; data(:)]); %Assuming basic state to be 0
    onSet = time(find(D == 1));
    offSet = time(find(D == -1));
             
    %If offset was not recorded, add it
    if length(onSet) ~= length(offSet)
        fprintf('Different number of onsets(%d) and offsets(%d)\n', length(onSet), length(offSet));
        if length(onSet) == (length(offSet)+1)
            tEnd = time(end);
            fprintf('Setting offset to protocol end %f\n', tEnd);
            offSet = [offSet; tEnd];
        else
            warning('No way to fix this. Will only import as many complet periods as present')
        end
        
        nP = min(length(onSet), length(offSet));
        onSet = onSet(1:nP);
        offSet = offSet(1:nP);    
    end
end


function newT = findColumnTypes(T, typeDefMap)
    newT = table();
    K = typeDefMap.keys(); % A cell array containing all names/keys
    for column = T.Properties.VariableNames
        column = column{1};
        %m = strmatch(column, K);
        m = findPattern(column, K);
                
        switch length(m)
            case 0                
                type = detectTypeByValues(T{:, column});
                fprintf('For column %s detected type %c\n', column, type);
                tT = table(T{:, column}, 'VariableNames', {column});
                tT.Properties.VariableUnits = {type};
                newT = [newT tT];
            case 1                            
                type = typeDefMap(K{m});
                tT = table(T{:, column}, 'VariableNames', {column});
                tT.Properties.VariableUnits = {type};
                newT = [newT tT];
            otherwise
                warning('Multiple typeDefMatches for column %s. Will skipp it!', column);                
        end        
    end        
end

function matchPosition = findPattern(searchTerm, patterns)
    matchPosition = [];
    
    m = regexp(searchTerm, patterns); %Returns the match position for each regular expression in patterns
    %Get the position of the pattern matched, if any
    for i = 1:length(patterns)
        if ~isempty(m{i})
            matchPosition = [matchPosition i];
        end
    end    
end

function type = detectTypeByValues(column)
    u = unique(column);
    ul = length(u);
    if ul <= 2
        type = 'B';
    elseif ul > 2
        type = 'C';
    else
        type = '?';
    end
end

function l = getLine(fileDescriptor)
    f = fileDescriptor;
    while ~feof(f)
        l = fgetl(f);
        %Filter empty lines
        if isempty(l)
            continue;
        else
            return;
        end
    end
    l = [];
end

function T = readTraceFile(traceFile)
try
    f = fopen(traceFile);
    
    %% Read Header
    stillHeader = true;
    fields = {};
    values = {};
    lineCnt = 0;
    while stillHeader
        l = getLine(f);
        s = textscan(l, '%q', 'Delimiter', ';');
        %n = length(s{1});
        if ~strcmp(s{1}{1}, ' ')
            %Normal field
            fields{end+1} = s{1}{1};
            try
                values{end+1} = s{1}{2};
            catch 
                values{end+1} = '';
            end
        else
            %last field
            stillHeader = false;
        end
        lineCnt = lineCnt + 1;
    end
    l = getLine(f);
    columnNames = textscan(l, '%q', 'Delimiter', ';');
    columnNames = columnNames{1};
    lineCnt = lineCnt + 1;
    l = getLine(f);
    units = textscan(l, '%q', 'Delimiter', ';');
    units = units{1};
    lineCnt = lineCnt + 1;
    
    H = table(fields', values', 'VariableNames', {'Fields', 'Value'});
        
    %data = table([], 'VariableNames', columnNames);
    %% Read the actual data    
    data = {};
    l = getLine(f);
    while ~feof(f)        
        s = textscan(l, '%q', 'Delimiter', ';');        
        %Convert the strings in the cell array to double, or NaN
        p = str2double(s{1}');
        data(end+1, :) = mat2cell(p, [1], ones(1,length(columnNames)));
        l = getLine(f);
    end
    
    T = cell2table(data, 'VariableNames', matlab.lang.makeValidName(columnNames'));
    
    %% Detect Column type
    nColumns = length(T.Properties.VariableNames);
    units = {};
    units{1} = 'T';
    units{2} = 'T';
    for i = 3:nColumns
        u = unique(T.(i));
        ul = length(u);
        if ul <= 2
            units{end+1} = 'B';
        elseif ul > 10
            units{end+1} = 'C';
        else
            units{end+1} = '?';
        end
    end
    T.Properties.VariableUnits = units;
catch err
    error('Failed! %s', err.message)
end
end


%% Helper Functions provided by Plexon
function [ nexFile ] = nexCreateFileData( timestampFrequency )
% [nexFile] = nexCreateFileData(timestampFrequency) -- creates empty nex file data structure
%
% INPUT:
%   timestampFrequency - timestamp frequency in Hertz
%
    nexFile.version = 100;
    nexFile.comment = '';
    nexFile.freq = timestampFrequency;
    nexFile.tbeg = 0;
    % fake end time of 1 time tick. tend will be modified by nexAdd* functions
    nexFile.tend = 1/timestampFrequency; 
end

function [ nexFile ] = nexAddEvent( nexFile, timestamps, name )
% [nexFile] = nexAddEvent( nexFile, timestamps, name ) -- adds an event 
%             (series of timestamps) to nexFile data structure
%
% INPUT:
%   nexFile - nex file data structure created in nexCreateFileData
%   timestamps - vector of event timestamps in seconds
%   name - event name

    eventCount = 0;
    if(isfield(nexFile, 'events'))
        eventCount = size(nexFile.events, 1);
    end
    eventCount = eventCount+1;
    nexFile.events{eventCount,1}.name = name;
    nexFile.events{eventCount,1}.varVersion = 100;
    % timestamps should be a vector
    if size(timestamps,1) == 1
        % if row, transpose to vector
        nexFile.events{eventCount,1}.timestamps = timestamps';
    else
        nexFile.events{eventCount,1}.timestamps = timestamps;
    end
    % modify end of file timestamp value in file header
    nexFile.tend = max(nexFile.tend, timestamps(end));
end

function [ nexFile ] = nexAddContinuous( nexFile, startTime, adFreq, values, name )
% [nexFile] = nexAddContinuous( nexFile, startTime, adFreq, values, name ) 
%         -- adds continuous variable to nexFile data structure
%
% INPUT:
%   nexFile - nex file data structure created in nexCreateFileData
%   startTime - time of the first data point in seconds
%   adFreq - A/D sampling rate of continuous variable in samples per second
%   values - vector of continuous variable values in milliVolts
%   name - continuous variable name  
% 
    contCount = 0;
    if(isfield(nexFile, 'contvars'))
        contCount = size(nexFile.contvars, 1);
    end
    contCount = contCount+1;
    nexFile.contvars{contCount,1}.name = name;
    nexFile.contvars{contCount,1}.varVersion = 100;
    nexFile.contvars{contCount,1}.ADFrequency = adFreq;
    nexFile.contvars{contCount,1}.timestamps = startTime;
    nexFile.contvars{contCount,1}.fragmentStarts = 1;
    nexFile.contvars{contCount,1}.data = values;
    
    % values should be a vector
    if size(values,1) == 1
        % if row, transpose to vector
        nexFile.contvars{contCount,1}.data = values';
    else
        nexFile.contvars{contCount,1}.data = values;
    end
    
    % modify end of file timestamp value in file header
    nexFile.tend = max(nexFile.tend, startTime+(max(size(values))-1)/adFreq);
end

function [result] = writeNex5File(nexFile, fileName)
% [result] = writeNex5File(nexFile, fileName) -- write nexFile structure
% to the specified .nex5 file. returns 1 if succeeded, 0 if failed.
% 
% INPUT:
%   nexFile - a structure containing .nex file data
%
%           SOME FIELDS OF THIS STRUCTURE ARE NOT DESCRIBED
%           BELOW. IT IS RECOMMENDED THAT YOU READ A VALID .NEX FILE 
%           TO FILL THIS STRUCTURE, THEN MODIFY THE STRUCTURE AND SAVE IT.
%
%           IF YOU WANT TO CREATE NEW .NEX FILE, USE nexCreateFileData.m,
%           nexAddContinuous.m etc. See exampleSaveDataInNexFile.m.
%           
%   fileName - if empty string, will use File Save dialog
%
%   nexFile - a structure containing .nex file data
%   nexFile.version - file version
%   nexFile.comment - file comment
%   nexFile.tbeg - beginning of recording session (in seconds)
%   
%
%   nexFile.neurons - array of neuron structures
%           neurons{i}.name - name of a neuron variable
%           neurons{i}.timestamps - array of neuron timestamps (in seconds)
%               to access timestamps for neuron 2 use {n} notation:
%               nexFile.neurons{2}.timestamps
%
%   nexFile.events - array of event structures
%           events{i}.name - name of event variable
%           events{i}.timestamps - array of event timestamps (in seconds)
%
%   nexFile.intervals - array of interval structures
%           intervals{i}.name - name of interval variable
%           intervals{i}.intStarts - array of interval starts (in seconds)
%           intervals{i}.intEnds - array of interval ends (in seconds)
%
%   nexFile.waves - array of wave structures
%           waves{i}.name - name of waveform variable
%           waves{i}.NPointsWave - number of data points in each wave
%           waves{i}.WFrequency - A/D frequency for wave data points
%           waves{i}.timestamps - array of wave timestamps (in seconds)
%           waves{i}.waveforms - matrix of waveforms (in milliVolts), each
%                             waveform is a column 
%
%   nexFile.contvars - array of continuous variable structures
%           contvars{i}.name - name of continuous variable
%           contvars{i}.ADFrequency - A/D frequency for data points
%
%           Continuous (a/d) data for one channel is allowed to have gaps 
%           in the recording (for example, if recording was paused, etc.).
%           Therefore, continuous data is stored in fragments. 
%           Each fragment has a timestamp and an index of the first data 
%           point of the fragment (data values for all fragments are stored
%           in one array and the index indicates the start of the fragment
%           data in this array).
%           The timestamp corresponds to the time of recording of 
%           the first a/d value in this fragment.
%
%           contvars{i}.timestamps - array of timestamps (fragments start times in seconds)
%           contvars{i}.fragmentStarts - array of start indexes for fragments in contvar.data array
%           contvars{i}.data - array of data points (in milliVolts)
%
%   nexFile.popvectors - array of population vector structures
%           popvectors{i}.name - name of population vector variable
%           popvectors{i}.weights - array of population vector weights
%
%   nexFile.markers - array of marker structures
%           markers{i}.name - name of marker variable
%           markers{i}.timestamps - array of marker timestamps (in seconds)
%           markers{i}.values - array of marker value structures
%               markers{i}.value.name - name of marker value 
%               markers{i}.values{j}.strings - array of marker value
%                     strings (if values are stored as strings in the file)
%               markers{i}.values{j}.numericValues - numeric marker values
%                     (if values are stored as numbers in the file)
%

result = 0;

if (nargin < 2 || isempty(fileName))
   [fname, pathname] = uiputfile('*.nex5', 'Save file name');
    if isequal(fname,0)
     error 'File name was not selected'
     return
   end
   fileName = fullfile(pathname, fname);
end

% note 'l' option when opening the file. 
% this options means that the file is 'little-endian'.
% this should ensure that the files are written correctly 
% on big-endian systems, such as Mac G5.
fid = fopen(fileName, 'w', 'l', 'US-ASCII');
if(fid == -1)
   error 'Unable to open file'
   return
end

% count all the variables
neuronCount = 0;
eventCount = 0;
intervalCount = 0;
waveCount = 0;
contCount = 0;
markerCount = 0;

if(isfield(nexFile, 'neurons'))
    neuronCount = size(nexFile.neurons, 1);
end
if(isfield(nexFile, 'events'))
    eventCount = size(nexFile.events, 1);
end
if(isfield(nexFile, 'intervals'))
    intervalCount = size(nexFile.intervals, 1);
end
if(isfield(nexFile, 'waves'))
    waveCount = size(nexFile.waves, 1);
end
if(isfield(nexFile, 'contvars'))
    contCount = size(nexFile.contvars, 1);
end
if(isfield(nexFile, 'markers'))
    markerCount = size(nexFile.markers, 1);
end

nvar = int32(neuronCount+eventCount+intervalCount+waveCount+contCount+markerCount);

% write header information
fwrite(fid, 894977358, 'int32');
fwrite(fid, 500, 'int32'); % for now, using version 500 for all headers
writeStringPaddedWithZeros(fid, nexFile.comment, 256);
fwrite(fid, nexFile.freq, 'double');
fwrite(fid, int64(nexFile.tbeg*nexFile.freq), 'int64');
fwrite(fid, nvar, 'int32');
% write zero meta offset
fwrite(fid, 0, 'uint64');

% skip padding
fwrite(fid, char(zeros(1, 64)), 'char');

% calculate where variable data starts
dataOffset = 356 + nvar*244;

% write variable headers

varHeader.Type = 0;
varHeader.Version = 500;
varHeader.Name = ' ';
varHeader.DataOffset = 0;
varHeader.Count = 0;    
varHeader.TimestampDataType = 0;
varHeader.ContinuousDataType = 0;
varHeader.SamplingFrequency = 0;
varHeader.Units = 'mV';
varHeader.ADtoUnitsCoefficient = 0;
varHeader.UnitsOffset  = 0;
varHeader.NumberOfDataPoints = 0;
varHeader.PrethresholdTimeInSeconds = 0;
varHeader.MarkerDataType = 0;
varHeader.NumberOfMarkerFields = 0;
varHeader.MarkerLength = 0;
varHeader.ContinuousIndexOfFirstPointInFramgmentDataType = 0;

% write neuron headers
for i = 1:neuronCount
    varHeader.Type = 0;
    varHeader.Name = nexFile.neurons{i}.name;
    varHeader.Count = size(nexFile.neurons{i}.timestamps,1);
    varHeader.DataOffset = dataOffset;
    
    writeNex5VarHeader(fid, varHeader);
    
    dataOffset = dataOffset + varHeader.Count*4;
end
   
% event headers
for i = 1:eventCount
    varHeader.Type = 1;
    varHeader.Name = nexFile.events{i}.name;
    varHeader.Count = size(nexFile.events{i}.timestamps,1);
    varHeader.DataOffset = dataOffset;
    
     writeNex5VarHeader(fid, varHeader);
    
    dataOffset = dataOffset + varHeader.Count*4;
end
    
% interval headers
for i = 1:intervalCount
    varHeader.Type = 2;
    varHeader.Name = nexFile.intervals{i}.name;
    varHeader.Count = size(nexFile.intervals{i}.intStarts,1);
    varHeader.DataOffset = dataOffset;
    
    writeNex5VarHeader(fid, varHeader);
    
    dataOffset = dataOffset + varHeader.Count*8;
end

% wave headers
for i = 1:waveCount
    % we need to recalculate a/d to milliVolts factor
    wmin = min(min(nexFile.waves{i}.waveforms));
    wmax = max(max(nexFile.waves{i}.waveforms));
    c = max(abs(wmin),abs(wmax));
    if (c == 0)
        c = 1;
    else
        c = c/32767;
    end
    nexFile.waves{i}.ADtoMV = c;
    nexFile.waves{i}.MVOfffset = 0;
    
    varHeader.Type = 3;
    varHeader.Name = nexFile.waves{i}.name;
    varHeader.Count = size(nexFile.waves{i}.timestamps,1);
    varHeader.DataOffset = dataOffset;
    varHeader.SamplingFrequency = nexFile.waves{i}.WFrequency;
    varHeader.ADtoUnitsCoefficient = nexFile.waves{i}.ADtoMV;
    varHeader.UnitsOffset = nexFile.waves{i}.MVOfffset;
    varHeader.NumberOfDataPoints = nexFile.waves{i}.NPointsWave;
    
    writeNex5VarHeader(fid, varHeader);
    
    dataOffset = dataOffset + varHeader.Count*4 + varHeader.NumberOfDataPoints*varHeader.Count*2;
end
 
% continuous variables
for i = 1:contCount
    % we need to recalculate a/d to milliVolts factor
    wmin = min(min(nexFile.contvars{i}.data));
    wmax = max(max(nexFile.contvars{i}.data));
    c = max(abs(wmin),abs(wmax));
    if (c == 0)
        c = 1;
    else
        c = c/32767;
    end
    nexFile.contvars{i}.ADtoMV = c;
    nexFile.contvars{i}.MVOfffset = 0;
      
    varHeader.Type = 5;
    varHeader.Name = nexFile.contvars{i}.name;
    varHeader.Count = size(nexFile.contvars{i}.timestamps,1);
    varHeader.DataOffset = dataOffset;
    varHeader.SamplingFrequency = nexFile.contvars{i}.ADFrequency;
    varHeader.ADtoUnitsCoefficient = nexFile.contvars{i}.ADtoMV;
    varHeader.UnitsOffset = nexFile.contvars{i}.MVOfffset;
    varHeader.NumberOfDataPoints = size(nexFile.contvars{i}.data, 1);
    
    writeNex5VarHeader(fid, varHeader);
    
    dataOffset = dataOffset + varHeader.Count*8 + varHeader.NumberOfDataPoints*2;
end

% markers
for i = 1:markerCount
    varHeader.MarkerDataType = 0;
    
    nexFile.markers{i}.NMarkers = size(nexFile.markers{i}.values, 1);
    nexFile.markers{i}.MarkerLength = 0;  
    if (nexFile.markers{i}.NMarkers > 0)
        % check the first marker field
        if(isfield(nexFile.markers{i}.values{1,1}, 'numericValues')) 
            varHeader.MarkerDataType = 1;
        end
    end
    if (varHeader.MarkerDataType == 0)
        MarkerLength = 0;
        for j = 1:nexFile.markers{i}.NMarkers
          for k = 1:size(nexFile.markers{i}.values{j,1}.strings, 1)
            MarkerLength = max(MarkerLength, size(nexFile.markers{i}.values{j,1}.strings{k,1}, 2));
          end
        end
        % add extra char to hold zero (end of string)
        MarkerLength = MarkerLength + 1;
        nexFile.markers{i}.MarkerLength = MarkerLength;    
    end    
    
    varHeader.Type = 6;
    varHeader.Name = nexFile.markers{i}.name;
    varHeader.Count = size(nexFile.markers{i}.timestamps,1);
    varHeader.DataOffset = dataOffset;
    varHeader.SamplingFrequency = 0;
    varHeader.ADtoUnitsCoefficient = 0;
    varHeader.UnitsOffset = 0;
    varHeader.NumberOfDataPoints = 0;
    varHeader.NumberOfMarkerFields = nexFile.markers{i}.NMarkers;
    varHeader.MarkerLength = nexFile.markers{i}.MarkerLength;
    
    writeNex5VarHeader(fid, varHeader);
    if (varHeader.MarkerDataType == 0)
        dataOffset = dataOffset + varHeader.Count*4 + varHeader.NumberOfMarkerFields*64 + varHeader.NumberOfMarkerFields*varHeader.Count*varHeader.MarkerLength;    
    else
        % we have 4 bytes per marker value
        dataOffset = dataOffset + varHeader.Count*4 + varHeader.NumberOfMarkerFields*64 + varHeader.NumberOfMarkerFields*varHeader.Count*4; 
    end
end


for i = 1:neuronCount
    fwrite(fid, nexFile.neurons{i}.timestamps.*nexFile.freq, 'int32');
end
for i = 1:eventCount
    fwrite(fid, nexFile.events{i}.timestamps.*nexFile.freq, 'int32');
end
for i = 1:intervalCount
    fwrite(fid, nexFile.intervals{i}.intStarts.*nexFile.freq, 'int32');
    fwrite(fid, nexFile.intervals{i}.intEnds.*nexFile.freq, 'int32');
end
for i = 1:waveCount
    fwrite(fid, nexFile.waves{i}.timestamps.*nexFile.freq, 'int32');
    wf = int16(nexFile.waves{i}.waveforms./nexFile.waves{i}.ADtoMV);
    fwrite(fid, wf, 'int16');
end
for i = 1:contCount
    fwrite(fid, nexFile.contvars{i}.timestamps.*nexFile.freq, 'int32');
    fwrite(fid, nexFile.contvars{i}.fragmentStarts - 1, 'int32');
    fwrite(fid, int16(nexFile.contvars{i}.data./nexFile.contvars{i}.ADtoMV), 'int16');
end

for i = 1:markerCount
    fwrite(fid, nexFile.markers{i}.timestamps.*nexFile.freq, 'int32');
    
    for j = 1:nexFile.markers{i}.NMarkers
      writeStringPaddedWithZeros(fid, nexFile.markers{i}.values{j,1}.name, 64);
      if( isfield(nexFile.markers{i}.values{j,1}, 'numericValues') ) 
          fwrite(fid, nexFile.markers{i}.values{j,1}.numericValues, 'uint32');           
      else
          for k = 1:size(nexFile.markers{i}.values{j,1}.strings, 1)
            writeStringPaddedWithZeros( fid, nexFile.markers{i}.values{j,1}.strings{k,1}, nexFile.markers{i}.MarkerLength );
          end    
      end      
    end
end

fclose(fid);
result = 1;
end

function [ nexFile ] = nexAddInterval( nexFile, intervalStarts, intervalEnds, name )
% [nexFile] = nexAddInterval( nexFile, intervalStarts, intervalEnds, name ) 
%              -- adds an interval variable (series of intervals) to nexFile data structure
%
% INPUT:
%   nexFile - nex file data structure created in nexCreateFileData
%   intervalStarts - a vector of interval starts in seconds
%   intervalEnds - a vector of interval ends in seconds
%   name - interval variable name
    intervalCount = 0;
    if(isfield(nexFile, 'intervals'))
        intervalCount = size(nexFile.intervals, 1);
    end
    intervalCount = intervalCount+1;
    nexFile.intervals{intervalCount,1}.name = name;
    nexFile.intervals{intervalCount,1}.varVersion = 100;
    if size(intervalStarts,1) == 1
        % if row, transpose to vector
        nexFile.intervals{intervalCount,1}.intStarts = intervalStarts';
    else
        nexFile.intervals{intervalCount,1}.intStarts = intervalStarts;
    end
    if size(intervalEnds,1) == 1
        % if row, transpose to vector
        nexFile.intervals{intervalCount,1}.intEnds = intervalEnds';
    else
        nexFile.intervals{intervalCount,1}.intEnds = intervalEnds;
    end
    % modify end of file timestamp value in file header
    nexFile.tend = max(nexFile.tend, intervalEnds(end));
end

function [] = writeStringPaddedWithZeros(fid, theString, totalLength)
% [] = writeStringPaddedWithZeros(fid, theString, length) -- write the
% string padded with zeros to the specified  file. 
% 
% INPUT:
%   fid - file id          
%   theString - string to write
%   totalLength - total number of bytes to be written

    fwrite(fid, theString, 'char');
    padding = char(zeros(1, totalLength - length(theString)));
    fwrite(fid, padding, 'char'); 
end


function [] = writeNex5VarHeader(fid, varHeader)
% [] = writeNex5VarHeader(fid, varHeader) -- write nex5VarHeader structure
% to the specified .nex5 file. 
% 
% INPUT:
%   fid - file id
%           
%   varHeader - nex5VarHeader structure

    fwrite(fid, varHeader.Type, 'int32');
    fwrite(fid, varHeader.Version, 'int32');
    writeStringPaddedWithZeros(fid, varHeader.Name, 64);
    fwrite(fid, varHeader.DataOffset, 'uint64');
    fwrite(fid, varHeader.Count, 'uint64');    
    fwrite(fid, varHeader.TimestampDataType, 'int32');
    fwrite(fid, varHeader.ContinuousDataType, 'int32');
    fwrite(fid, varHeader.SamplingFrequency, 'double');
    writeStringPaddedWithZeros(fid, varHeader.Units, 32);
    fwrite(fid, varHeader.ADtoUnitsCoefficient, 'double');
    fwrite(fid, varHeader.UnitsOffset, 'double');
    fwrite(fid, varHeader.NumberOfDataPoints, 'uint64');
    fwrite(fid, varHeader.PrethresholdTimeInSeconds, 'double');
    fwrite(fid, varHeader.MarkerDataType, 'int32');
    fwrite(fid, varHeader.NumberOfMarkerFields, 'int32');
    fwrite(fid, varHeader.MarkerLength, 'int32');
    fwrite(fid, varHeader.ContinuousIndexOfFirstPointInFramgmentDataType, 'int32');
    fwrite(fid, char(zeros(1, 60)), 'char');
end

function [ nexFile ] = nexAddNeuron( nexFile, timestamps, name )
% [nexFile] = nexAddNeuron( nexFile, timestamps, name ) -- adds a neuron 
%             to nexFile data structure
%
% INPUT:
%   nexFile - nex file data structure created in nexCreateFileData
%   timestamps - vector of neuron timestamps in seconds
%   name - neuron name
%
    neuronCount = 0;
    if(isfield(nexFile, 'neurons'))
        neuronCount = size(nexFile.neurons, 1);
    end
    neuronCount = neuronCount+1;
    nexFile.neurons{neuronCount,1}.name = name;
    nexFile.neurons{neuronCount,1}.varVersion = 100;
    nexFile.neurons{neuronCount,1}.wireNumber = 0;
    nexFile.neurons{neuronCount,1}.unitNumber = 0;
    nexFile.neurons{neuronCount,1}.xPos = 0;
    nexFile.neurons{neuronCount,1}.yPos = 0;
    % timestamps should be a vector
    if size(timestamps,1) == 1
        % if row, transpose to vector
        nexFile.neurons{neuronCount,1}.timestamps = timestamps';
    else
         nexFile.neurons{neuronCount,1}.timestamps = timestamps;
    end
    % modify end of file timestamp value in file header
    nexFile.tend = max(nexFile.tend, timestamps(end));
end