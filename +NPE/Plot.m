classdef Plot   
     methods (Static)
        
         function [ fig ] = plotTracesTable(traces, horSpace, titleString)
            [r, c] = size(traces);            
            fig = figure('visible','off');
            hold on
            title(titleString)
            grid on
            xlabel('Time')
            ylabel('Traces')
            %set(gca, 'YTickLabel', traces.Properties.VariableNames)
            for l = 2:c
                plot(traces.(1), l*horSpace + traces.(l))
            end
        end
         
     end
end