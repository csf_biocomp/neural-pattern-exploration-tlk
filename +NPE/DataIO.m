classdef DataIO
     methods (Static)
        
         function T = readNeuroTraces(neuroData, columnNamePrefix)
            try
                data =  csvread(neuroData);
                [r, c] = size(data);    
                names = cell(c, 1);
                names{1} = [columnNamePrefix 'Time'];
                for i = 2:c
                    names{i} = [columnNamePrefix '_' num2str(i-1, '%03d')];
                end    
                %T = table(data, 'VariableNames', names);   
                T = array2table(data, 'VariableNames', names);
            catch err
                warning('Adding Neuron Traces failed! %s', err.message)
            end
         end
         
         
        function T = readNeuronEvents(neuroData, columnNamePrefix, eventNamePrefix)
            try
                import NPE.DataIO
                T = DataIO.readNeuroTraces(neuroData, columnNamePrefix);
                %Generate Events for each NeuroData
                eventT = table();
                for i = 2:width(T)
                    l = T.(i) > 0; %Logic
                    e = 0 + l; % 0, 1 Double
                    t = array2table(e, 'VariableNames', {sprintf('%s_%03d', eventNamePrefix, i-1)});
                    eventT = [eventT t];
                end
                T = [T eventT];    
            catch err
                warning('Adding Neuronal data failed! %s', err.message)
            end
        end
        
        function converted = softStr2Double(cellList)
            f = str2double(cellList);
            %converted = num2cell(f, [1], ones(1, length(f)));
            converted = num2cell(f);
            notDouble = find( isnan(f) == 1);
            if ~isempty(notDouble)
                converted{notDouble} = cellList(notDouble);            
            end
        end
        
        function [T] = readCSV(csvFile, varargin)
        try
            import NPE.DataIO
            
            p = inputParser;
            addRequired(p,'filename',@ischar);
            addOptional(p,'sep',';',@(x) ischar(x) && (length(x) == 1))
            addOptional(p,'containsHeader',true,@islogical)
            p.parse(csvFile, varargin{:})
            filename = p.Results.filename;
            sep = p.Results.sep;
            containsHeader = p.Results.containsHeader;
            
            f = fopen(filename);

            %% Read Header   
            fields = {};
            values = {};
            lineCnt = 0;
            if containsHeader
                l = fgetl(f);
                header = textscan(l, '%q', 'Delimiter', sep);
                fields = header{1};
                c = length(fields);
            else
                l = fgetl(f);
                header = textscan(l, '%q', 'Delimiter', sep);                
                c = length(header);
                
                fields = {};
                for i = 1:c
                    fields{i} = sprintf('Column_%02d', i); 
                end
                %Reset file identifier
                fseek(f, 0, 'bof');                
            end
            
%            H = table(fields', values', 'VariableNames', {'Fields', 'Value'});

            %data = table([], 'VariableNames', columnNames);
            %% Read the actual data    
            data = {};
            while ~feof(f)
                try
                    l = fgetl(f);
                    s = textscan(l, '%q', 'Delimiter', sep);        
                    %Convert the strings in the cell array to double, or NaN
                    %p = str2double(s{1}');
                    p = NPE.DataIO.softStr2Double(s{1}');
                    data(end+1, :) = p;        
                catch err
                    warning('Processing line failed!');
                end
            end

            T = cell2table(data, 'VariableNames', matlab.lang.makeValidName(fields'));           
        catch err
            error('Failed! %s', err.message)
        end
        end
         
     end
end