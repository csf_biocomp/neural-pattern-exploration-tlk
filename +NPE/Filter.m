classdef Filter   
     methods (Static)
        
         function [ res ] = expConvolution(traces, tau)
         % Calculates the convulution of a signal with an exponential
         % decaying step function
         %
%          import NPE.Filter
%          z = zeros(40,1);
%          o = ones(40,1);
%          traces = vertcat(-1 * o, o, z, o, -1 * o);
%          conv = NPE.Filter.expConvolution(traces, 10);
%          plot(traces)
%          hold on
%          grid on
%          plot(conv)
%          legend('Signal', 'Convoluted Result')
             N = tau * 4;
             t = 0:(N-1);             
             %Zero Pad around signal to remove artifactes
             T = vertcat(zeros(N, 1), traces, zeros(N, 1));
             lambda = 1.0 / tau;
             g = 1.0 * exp(-t * lambda);             
             nT = T / max(T);
             res = conv(nT, g);
             res = res(N:end-(2*N));
             res = res / max(res);
         end
         
        function trend = getTrend(X, windowSize)
            assert(windowSize < length(X))

            m = median(X(1:(windowSize*1.0)));
            t1 = repmat(m, windowSize*1.0, 1);
            m2 = median(X(end-(windowSize*1.0):end));
            t2 = repmat(m2, windowSize*1.0, 1);
            data = vertcat(t1, X, t2);

            smoother = fspecial('average', [1, windowSize]);

            trend = filter(smoother, 1, data);

            %Cut off beginning of trend, and shift it by windowSize/2
            %trend = trend(windowSize+1:end-(windowSize/2));
            %trend = trend(windowSize+1:end);
            trend = trend((windowSize*1.5)+1:end-(windowSize * 0.5));
        end

        function compensated = comprensateForTrend(X, trend)
            assert(length(X) == length(trend))

            baseline = mean(trend);
            scaling = trend / baseline;

            compensated = X - trend;
            compensated = compensated .* scaling;    

            m = min(compensated);
            compensated = compensated + abs(m);
        end
        
        function [ filtered ] = DoG(signal, smallGaussWindowSize, bigGaussWindowSize)
            assert(bigGaussWindowSize > smallGaussWindowSize)
            assert(smallGaussWindowSize < length(signal))
            
            N = bigGaussWindowSize;
            T = vertcat(zeros(N, 1), signal, zeros(N, 1));                        
            
            bigG = fspecial('gaussian', [1, bigGaussWindowSize], bigGaussWindowSize / 8.0);
            smallG = fspecial('gaussian', [1, smallGaussWindowSize], smallGaussWindowSize / 8.0);
            
            smallFiltered = conv(T, smallG);
            bigFiltered = conv(T, bigG);
            
            smallFiltered = smallFiltered(N+length(smallG)/2:end-(N+length(smallG)/2));
            bigFiltered = bigFiltered(N+length(bigG)/2:end-(N+length(bigG)/2));
            
            %wD = (bigGaussWindowSize - smallGaussWindowSize)/2;
            %bigFiltered = bigFiltered(wD:end-(wD+1));
            filtered = smallFiltered - bigFiltered;
            %filtered = filtered(N*2:end-(N*2));
            filtered = filtered / max(filtered);          
        end
        
        function [ events ] = DoGEventDetection(signal, smallGaussWindowSize, bigGaussWindowSize, stdScaling, eventCenter)
% import NPE.Filter
% N = 100;
% z = zeros(N,1);
% u = [1:N]';
% trace  = vertcat(u, z, -1*u, z);
% p = NPE.Filter.DoGEventDetection(trace, 10, N, 3.0, 'valley');
% plot(trace)
% hold on
% scatter(e, zeros(length(e), 1))
            import NPE.Filter;
            assert(bigGaussWindowSize > smallGaussWindowSize)
            assert(smallGaussWindowSize < length(signal))
            
            %Wrap the signal with zeros to get no artifcats from the convolution
            N = bigGaussWindowSize;
            T = vertcat(zeros(N, 1), signal, zeros(N, 1));                        
            
            bigG = fspecial('gaussian', [1, bigGaussWindowSize], bigGaussWindowSize / 8.0);
            smallG = fspecial('gaussian', [1, smallGaussWindowSize], smallGaussWindowSize / 8.0);
            
            smallFiltered = conv(T, smallG);
            bigFiltered = conv(T, bigG);
            
            %Get the 'relevant' signal from the convoluted signal            
            sWD = floor(N+length(smallG)/2);
            smallFiltered = smallFiltered(sWD:end-sWD);
            bWD = floor(N+length(bigG)/2);
            bigFiltered = bigFiltered(bWD:end-bWD);
            
            %Makre sure the arrays are of the same length
            WD = min(length(smallFiltered), length(bigFiltered));
            
            %Substract the blured signals and find peakes that are some
            %magnitues higher than the local stdandart deviation
            posSignal = smallFiltered(1:WD) - bigFiltered(1:WD);            
            posSignal = posSignal .* (posSignal > 0);
            movingStd = NPE.Filter.movingstd(posSignal, bigGaussWindowSize, 'central');
            high = posSignal > (movingStd .* stdScaling);
                        
            %Return the center point of continous high blocks
            dH = diff(high);            
            p = find(dH == 1);
            n = find(dH == -1);
            if length(n) < length(p)
                n = [n; length(high)];
            end
            
            switch eventCenter
                case 'onSet'
                    events = p;
                case 'center'
                    events = p + floor((n - p) / 2);
                case 'offSet'
                    events = n;
                case 'valley'
                    %Find the first valley before the positiv threshold
                    D = [-1; diff(posSignal)];
                    valley = zeros(length(p), 1);
                    for i = 1:length(p)
                        pos = p(i);                        
                        for j = pos:-1:1
                            if D(j) <= 0
                                valley(i) = j;
                                break;
                            end
                        end                                               
                    end
                    events = valley;
            end
        end
        
        function [fEvents] = filterEvents(events, blindspot)
            % Filter Events, not allowing for a second event for blindspot
            % bins after an event
            if length(events) < 2
                fEvents = events;
            else    
                fEvents = [events(1)];
                last = events(1);
                for i = 2:length(events)
                    e = events(i);
                    if e >= (last + blindspot)
                        fEvents = [fEvents; e];
                        last = e;
                    end                    
                end
            end
        end
        
		% Copied from http://www.mathworks.com/matlabcentral/fileexchange/9428-movingstd-x-k-windowmode-
        function s = movingstd(x,k,windowmode)
            % movingstd: efficient windowed standard deviation of a time series
            % usage: s = movingstd(x,k,windowmode)
            %
            % Movingstd uses filter to compute the standard deviation, using
            % the trick of std = sqrt((sum(x.^2) - n*xbar.^2)/(n-1)).
            % Beware that this formula can suffer from numerical problems for
            % data which is large in magnitude. Your data is automatically
            % centered and scaled to alleviate these problems.
            %
            % At the ends of the series, when filter would generate spurious
            % results otherwise, the standard deviations are corrected by
            % the use of shorter window lengths.
            %
            % arguments: (input)
            %  x   - vector containing time series data
            %
            %  k   - size of the moving window to use (see windowmode)
            %        All windowmodes adjust the window width near the ends of
            %        the series as necessary.
            %
            %        k must be an integer, at least 1 for a 'central' window,
            %        and at least 2 for 'forward' or 'backward'
            %
            %  windowmode - (OPTIONAL) flag, denotes the type of moving window used
            %        DEFAULT: 'central'
            %
            %        windowmode = 'central' --> use a sliding window centered on
            %            each point in the series. The window will have total width
            %            of 2*k+1 points, thus k points on each side.
            %        
            %        windowmode = 'backward' --> use a sliding window that uses the
            %            current point and looks back over a total of k points.
            %        
            %        windowmode = 'forward' --> use a sliding window that uses the
            %            current point and looks forward over a total of k points.
            %
            %        Any simple contraction of the above options is valid, even
            %        as short as a single character 'c', 'b', or 'f'. Case is
            %        ignored.
            %
            % arguments: (output)
            %  s   - vector containing the windowed standard deviation.
            %        length(s) == length(x)

            % check for a windowmode
            if (nargin<3) || isempty(windowmode)
              % supply the default:
              windowmode = 'central';
            elseif ~ischar(windowmode)
              error 'If supplied, windowmode must be a character flag.'
            end
            % check for a valid shortening.
            valid = {'central' 'forward' 'backward'};
            ind = find(strncmpi(windowmode,valid,length(windowmode)));
            if isempty(ind)
              error 'Windowmode must be a character flag, matching the allowed modes: ''c'', ''b'', or ''f''.'
            else
              windowmode = valid{ind};
            end

            % length of the time series
            n = length(x);

            % check for valid k
            if (nargin<2) || isempty(k) || (rem(k,1)~=0)
              error 'k was not provided or not an integer.'
            end
            switch windowmode
              case 'central'
                if k<1
                  error 'k must be at least 1 for windowmode = ''central''.'
                end
                if n<(2*k+1)
                  error 'k is too large for this short of a series and this windowmode.'
                end
              otherwise
                if k<2
                  error 'k must be at least 2 for windowmode = ''forward'' or ''backward''.'
                end
                if (n<k)
                  error 'k is too large for this short of a series.'
                end
            end

            % Improve the numerical analysis by subtracting off the series mean
            % this has no effect on the standard deviation.
            x = x - mean(x);
            % scale the data to have unit variance too. will put that
            % scale factor back into the result at the end
            xstd = std(x);
            x = x./xstd;

            % we will need the squared elements 
            x2 = x.^2;

            % split into the three windowmode cases for simplicity
            A = 1;
            switch windowmode
              case 'central'
                B = ones(1,2*k+1);
                s = sqrt((filter(B,A,x2) - (filter(B,A,x).^2)*(1/(2*k+1)))/(2*k));
                s(k:(n-k)) = s((2*k):end);
              case 'forward'
                B = ones(1,k);
                s = sqrt((filter(B,A,x2) - (filter(B,A,x).^2)*(1/k))/(k-1));
                s(1:(n-k+1)) = s(k:end);
              case 'backward'
                B = ones(1,k);
                s = sqrt((filter(B,A,x2) - (filter(B,A,x).^2)*(1/k))/(k-1));
            end

            % special case the ends as appropriate
            switch windowmode
              case 'central'
                % repairs are needed at both ends
                for i = 1:k
                  s(i) = std(x(1:(k+i)));
                  s(n-k+i) = std(x((n-2*k+i):n));
                end
              case 'forward'
                % the last k elements must be repaired
                for i = (k-1):-1:1
                  s(n-i+1) = std(x((n-i+1):n));
                end
              case 'backward'
                % the first k elements must be repaired
                for i = 1:(k-1)
                  s(i) = std(x(1:i));
                end
            end

            % catch any complex std elements due to numerical precision issues.
            % anything that came out with a non-zero imaginary part is
            % indistinguishable from zero, so make it so.
            s(imag(s) ~= 0) = 0;

            % restore the scale factor that was used before to normalize the data
            s = s.*xstd;
        end

     end
end