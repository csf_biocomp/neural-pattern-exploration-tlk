function [ output_args ] = Zauberkugel(featureFile, varargin)
%ZAUBERKUGEL Summary of this function goes here
%   Detailed explanation goes here
    VERSION = 1.0;
    fprintf('Running Zauberkugel v%.02f ... \n', VERSION);
    
    if nargin > 1
        if isnumeric(varargin{1})
            nComponents = varargin{1};
        else
            nComponents = str2num(varargin{1});
        end
    else
        nComponents = 2;
    end
    
    if (nComponents > 3) || (nComponents < 1) 
        error('nComponents must be between 1 and 3!')
    end
    
    fprintf('Plotting %d first PCA components ...\n', nComponents);
    
    import NPE.DataIO
    T = NPE.DataIO.readCSV(featureFile, ','); 
    X = T{:,2:end};
    
    %normalize the data uzing the zscore for each feature
    nX = zscore(X);   
    %nX = X;
    
    %[conv, score, latent, tsquares, explained] = pca(nX, 'Algorithm', 'als');
    [conv, score, latent, tsquares, explained] = pca(nX);
    
    [X, Y, Z, groups, labels, features] = getComponents(score, T);
    
    plotPCAStats(conv, explained, features, 10);
    
    f = figure();
    switch(nComponents)
        case 1
            varCoverage = explained(1);
            for i = 1:length(X)
                x = X(i);
                g = groups(i);
                l = labels(i);
                p = plot(x, 0,'o', 'DisplayName', g{:});
                %set(p, 'MarkerFaceColor', p.Color);
                p.MarkerFaceColor = p.Color;
                p.Color = 'black';
                hold('on');
                text(x, 0, l,  'horizontal', 'left', 'vertical', 'bottom');
            end            
            legend('show');
        case 2            
            varCoverage = explained(1) + explained(2);
            gscatter(X, Y, groups)
            text(X, Y, groups, 'horizontal', 'left', 'vertical', 'bottom')
            legend('show', 'Location', 'EastOutside');
        case 3                
            varCoverage = explained(1) + explained(2) + explained(3);
            for i = 1:length(Z)
                x = X(i); y = Y(i); z = Z(i);
                g = groups(i);
                %l = labels(i);
                l = groups(i);
                p = plot3(x,y,z,'o', 'DisplayName', g{:});
                %set(p, 'MarkerFaceColor', p.Color);
                p.MarkerFaceColor = p.Color;
                p.Color = 'black';
                hold('on');
                text(x, y, z, l,  'horizontal', 'left', 'vertical', 'bottom');
            end
            grid('on');
            set(gca, 'ZGrid', 'off');
            set(gca, 'GridLineStyle', '-.');
            legend('show');
            rotate3d on;
    end
    title(sprintf('First %d PCA, %02.0f%% Variation coverage', nComponents, varCoverage));
    
    %Some stats
    fprintf('%d first PCA components cover %f of the variation in all input space features.\n', nComponents, varCoverage);
    fn = 'PCA-components.csv';
    fprintf('Writing PCA components to %s ...', fn);
    csvwrite(fn, score);
end

function [X, Y, Z, groups, labels, features] = getComponents(score, T)
    
    ORIgroups = T{:, 1};
    features = T.Properties.VariableNames(2:end);
    features = reshape(features, length(features), 1);
    X = score(:,1);
    Y = score(:,2);
    Z = score(:,3);
    
    %Normalize PCA coordinates
    %comp = score(:, 1:3);
    %X = X / max(max(comp));
    %Y = Y / max(max(comp));
    %Z = Z / max(max(comp));
    
    groups = cell(length(ORIgroups), 1);
    labels = cell(length(ORIgroups), 1);
    for i = 1:length(ORIgroups)
        g = strrep(ORIgroups{i}, '_', '-');
        %groups{i} = sprintf('%d %s', i, g);
        groups{i} = sprintf('%s', g);
        labels{i} = num2str(i);
    end
end

function [] = plotPCAStats(PCAcomp, varExplenation, features, nRelevantFeatures)    
    nComponents = 3;
    
    nFeatures = min(nRelevantFeatures, length(features));
    iF = cell(nFeatures, nComponents);
    iCor = zeros(nFeatures, nComponents);
    
    %f = figure('visible','off');
    f = figure();
    subplot(2,2,1);
    %Plot Variance explanation by PCA component    
    bar(varExplenation);
    title(sprintf('Variance Explanation by PCA component'));
    ylim([0, 100]);
    ylabel('% of Variance Covered');
    xlabel('PCA Components');
    %pn = sprintf('PCA_VarianceExplanation.png');
    %fprintf('Writing Variance Explanation plot to %s ...\n', pn);
    %saveas(f, pn);
    %close(f);
    
    for i = 1:nComponents
        comp = PCAcomp(i,:);        
        [~, idx] = sort(abs(comp), 'descend');
        for j = 1:nFeatures
            iF{j,i} = features{idx(j)};
            iCor(j,i) = PCAcomp(i, idx(j));
        end
        %Generate the figures
        %f = figure('visible','off');
        subplot(2,2,i+1);
        bar(iCor(:,i));
        ax = gca;
        ax.XTick = 1:nFeatures;
        ax.XTickLabel = reshape(iF(:,i), 1, length(iF(:,i)));
        ax.XTickLabelRotation = 45;
        title(sprintf('%d PCA component. Highest Feature correlation', i));
        %pn = sprintf('%d_PCA_component.png', i);
        %fprintf('Writing PCA component plot to %s ...\n', pn);
        %saveas(f, pn);
        %close(f);
    end
    %saveas(f, 'PCA_Component_Analysis.png');
%    close(f);
end